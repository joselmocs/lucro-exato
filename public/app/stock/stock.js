(function () {
    'use strict';

    var app = angular.module('lucroExato.stock', []);

    app.config(['$stateProvider', function ($stateProvider) {
        $stateProvider
            .state("stock", {
                url: "/stock",
                views: {
                    main: {
                        templateUrl: 'app/stock/stock.html'
                    }
                }
            });
    }]);
}());