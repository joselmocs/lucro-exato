(function () {
    'use strict';

    var app = angular.module('lucroExato', [
        'ngSanitize',
        'ui.router',

        'lucroExato.dashboard',
        'lucroExato.stock'
    ]);

    app.config(['$stateProvider', '$urlRouterProvider', '$httpProvider',
        function ($stateProvider, $urlRouterProvider, $httpProvider) {
            // Define o content-type padrão para requisições feitas usando $http
            $httpProvider.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded; charset=UTF-8';
            $urlRouterProvider.otherwise('/dashboard');
        }]);
}());