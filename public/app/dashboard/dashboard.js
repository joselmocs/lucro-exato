(function () {
    'use strict';

    var app = angular.module('lucroExato.dashboard', []);

    app.config(['$stateProvider', function ($stateProvider) {
        $stateProvider
            .state("dashboard", {
                url: "/dashboard",
                views: {
                    main: {
                        templateUrl: 'app/dashboard/dashboard.html'
                    }
                }
            });
    }]);
}());