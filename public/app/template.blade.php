<!DOCTYPE html>
<html lang="en" ng-app="lucroExato">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="/favicon.ico">

    <title></title>

    <!-- Bootstrap core CSS -->
    <link href="/assets/libs/bootstrap-3.2.0-dist/css/bootstrap.min.css" rel="stylesheet">
    <link href="/assets/libs/bootstrap-3.2.0-dist/css/bootstrap-theme.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="/assets/css/custom.css" rel="stylesheet">

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>

<body>

<div class="navbar navbar-inverse navbar-fixed-top" role="navigation">
    <div class="container-fluid">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target=".navbar-collapse">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="#">Project name</a>
        </div>
        <div class="navbar-collapse collapse">
            <ul class="nav navbar-nav navbar-right">
                <li><a href="" ui-sref="dashboard">Dashboard</a></li>
                <li><a href="" ui-sref="stock">Stock</a></li>
                <li><a href="">Profile</a></li>
                <li><a href="">Help</a></li>
            </ul>
        </div>
    </div>
</div>

<div class="container-fluid">
    <div class="row">
        <div class="col-sm-3 col-md-2 sidebar">
            <ul class="nav nav-sidebar">
                <li class="active"><a href="#">Overview</a></li>
                <li><a href="#">Reports</a></li>
                <li><a href="#">Analytics</a></li>
                <li><a href="#">Export</a></li>
            </ul>
        </div>
        <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
            <div ui-view="main"></div>
        </div>
    </div>
</div>

<!-- Bootstrap core JavaScript
================================================== -->
<!-- Placed at the end of the document so the pages load faster -->
<script src="/assets/libs/jquery-2.1.1/jquery.min.js"></script>
<script src="/assets/libs/bootstrap-3.2.0-dist/js/bootstrap.min.js"></script>

<script src="/assets/libs/angular-1.2.26/angular.min.js"></script>
<script src="/assets/libs/angular-1.2.26/i18n/angular-locale_pt-br.js"></script>
<script src="/assets/libs/angular-1.2.26/angular-animate.min.js"></script>
<script src="/assets/libs/angular-1.2.26/angular-resource.min.js"></script>
<script src="/assets/libs/angular-1.2.26/angular-sanitize.min.js"></script>

<script src="/assets/libs/angular-ui-router-0.2.11/angular-ui-router.min.js"></script>

<script src="/app/app.js"></script>
<script src="/app/dashboard/dashboard.js"></script>
<script src="/app/stock/stock.js"></script>

</body>
</html>
